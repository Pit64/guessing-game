import { prompt } from "./prompt.js";

let targetNumber = Math.floor(Math.random() * 100);
let count = 0;

const game = () => {
  let number = Number(prompt("\nVeuillez insérer un chiffre entre 0 et 100 : "));

  count++;

  if (Number.isNaN(number)) {
    console.log("\nLe nombre inséré n'est pas un nombre.");
    return game();
  }

  if (!number) {
    console.log("\nLe nombre inséré est vide.");
    return game();
  }

  if (number < targetNumber) {
    console.log("\nLe nombre à trouver est plus grand !");
    return game();
  }

  if (number > targetNumber) {
    console.log("\nLe nombre à trouver est plus petit !");
    return game();
  }

  if (number === targetNumber) {
    console.log("\nBien joué, vous avez trouvé le bon nombre !");
    console.log(`Le nombre à trouver était ${targetNumber}.`);
    console.log(`Vous avez deviné le bon nombre en ${count} coups.`);
  }

  const reload = prompt("\nSouhaitez-vous rejouer ? ");

  if (reload === "o" || reload === "O") {
    targetNumber = Math.floor(Math.random() * 100);
    count = 0;
    return game();
  } else {
    console.log("\nMerci d'avoir joué !");
  }

  return;
};

console.log("Bienvenue au jeu du chiffre à deviner !\n\nLes règles :\n");
console.log("1. Le système va générer un bombre aléatoire entier entre 0 et 100.");
console.log("2. Votre tâche est de trouver ce nombre.");
console.log("3. Insérez un nombre quand il vous sera demandé.");
console.log("4. Si le nombre à trouver n'est pas bon, vous le saurez");
console.log("5. Le jeu continue jusqu'à trouver le bon nombre\n\n");
console.log("C'est parti !\n");

game();
